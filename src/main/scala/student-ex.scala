import scala.xml.Node
import main.scala.jdbc._
//import java.text.SimpleDateFormat
//import java.util.Calendar
import java.time.LocalDate
import java.time.format.DateTimeFormatter
  
object StudentEx extends App {

  val logger = LogObj.modLogger(this.getClass.getName)
  import java.nio.file.{Files,Paths}
  import java.nio.charset.Charset
//  import collection.JavaConverters._
  import scala.jdk.CollectionConverters._
  val ordnungFile = Paths.get("conf","ordnung.txt")
//  println("Path: " + ordnungFile)
  val cs = Charset.defaultCharset
  val ordnungText =
    if (Files.exists(ordnungFile)) Files.readAllLines(ordnungFile,cs).asScala.mkString("\n") + "\n\n"
    else ""
  
  def filterAndMoveTo(studListOpt: Option[Seq[Int]], targetOu: Ou.Value): Unit = {
    studListOpt.map(studList => {
      println("size studList: " + studList.length)
/*      for (id <- studList) {
        val exDate = H1.getStudentExDate(id)
//        println("id: " + id + " " + exDate + " " + H1.getStudentExUsername(id))
      }
*/
      val sl_1 = studList.filter(studId => {
        val username = H1.getStudentExUsername(studId)
//        println("working on studentId, username: " + studId + " " + username)
        (username.length > 0) && LdapDB.userExistsInOUList(username, Seq(Ou.Studenten, Ou.Studenten_ex))
      })
      println("after first filter (exists in OU Studenten or Studenten-ex?) new length: " + sl_1.length)
      for (id <- sl_1) {
        val username = H1.getStudentExUsername(id)
//        println("working on username: " + username)
        val actExpDate = LdapDB.getExpirationDate(username)
        val studentExDate = H1.getStudentExDate(id).plusMonths(12)
//        println("exmatriculation Date: " + studentExDate)
        if (studentExDate != actExpDate) {
          println("expDate from AD: " + actExpDate)
          LdapDB.setExpirationDate(username, studentExDate)
        }
      }
      val sl_2 = sl_1.filterNot(id => LdapDB.userExistsInOU(H1.getStudentExUsername(id), targetOu))
      println("length after second filter (exists in OU Student-ex?): " + sl_2.length)
      for (id <- sl_2) {
        val username = H1.getStudentExUsername(id)
        // students only start with euv
        if (username.startsWith("euv")) {
          LdapDB.moveUser(username, targetOu)
          import mail._
          val bccUser = Config.getProperty("Mail_bcc")
          val ccUser = H1.getPersonEmail(username).toSeq //.getOrElse("")
//          println(username + " has private Email: " + ccUser)

          if ((targetOu == Ou.Nach_2M) && sendMail) send a new Mail(
            from = ("no-reply@europa-uni.de", "AD-Info"),
//            to = "jschmidt@europa-uni.de",
            to = username + "@europa-uni.de",
            cc = ccUser,
            bcc = if (bccUser != "") Seq(bccUser) else Seq.empty,
            subject = "Hinweis zu Ihrer Exmatrikulation",
            message = "Sehr geehrter ehemaliger Studierender,\n\n" + 
              	  	"Ihre Exmatrikulation liegt mehr als 2 Monate zurück. " +
            		"Entsprechend der Nutzungsordnung der Viadrina werden Ihre Rechte auf die E-Mail Nutzung beschränkt.\n" +
            		"Nach Ablauf weiterer 4 Monate wird Ihr Account deaktiviert!\n\n" +
            		ordnungText +
            		"Ihr IKMZ"
          )
        }
        else println(s"bad username: $username")
      }
    })
//    H1.showCacheData
  }

  val dateForm = DateTimeFormatter.ofPattern("dd.MM.yyyy")
  val today = LocalDate.now

//  def main(args: Array[String]) {
  println("Start")

  val argLen = args.length
  if (argLen > 1) { println("use only one (-nomail) or no parameter"); sys.exit() }

  val noMail =
    if (argLen == 1) {
      args(0) match {
        case "-nomail" => true
        case _ => println("expected parameter: -nomail"); sys.exit()
      }
    }
    else false

  val sendMail = if (!noMail) Config.getProperty("nomail") != "true" else !noMail 
  if (!sendMail) println("Mails werden nicht versendet!")
  
  val Seq(cal_2M, cal_6M, cal_12M, cal_18M) = Seq(2,6,12,18).map(today.minusMonths(_).format(dateForm))
// dates are correct now

  filterAndMoveTo(H1.getStudentExList(cal_2M, cal_6M), Ou.Nach_2M)
  filterAndMoveTo(H1.getStudentExList(cal_6M, cal_12M), Ou.Nach_6M)
  filterAndMoveTo(H1.getStudentExList(cal_12M, cal_18M), Ou.Nach_12M)

// reverse search now
  val studExList = LdapDB.getAllFromOU(Ou.Studenten_ex).filter(username => {
    val isStudent = username.startsWith("euv")
    if (!isStudent) println(s"unexpected Username: $username")
    isStudent
  })
  println("size StudExList: " + studExList.length)
  for (username <- studExList) {
//      if (!username.startsWith("euv")) println("unexpected Username: " + username)
//      else println(username + " " + H1.getExmatriculationDate(username))
    val exmaDateOpt = H1.getExmatriculationDate(username)
      if (exmaDateOpt.isDefined) {
        val exmaDate = exmaDateOpt.get
        if (LdapDB.getExpirationDate(username) != exmaDate.plusMonths(12)) {
          println(s"username: $username has exmatrikulation date + 12M: " + exmaDate.plusMonths(12))
          println(s"username: $username has expirationDate from AD: " + LdapDB.getExpirationDate(username))
          logger.info(username + ": die Daten der Exmatrikulation in AD und H1 stimmen nicht überein!")
        }
      }
      else logger.info(s"username: $username hat kein Exmatrikulationsdatum in HisInOne oder existiert nicht!")
  }
//  }
}