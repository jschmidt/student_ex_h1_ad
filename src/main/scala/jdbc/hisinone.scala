package main.scala.jdbc
//import scala.xml.{Node,Elem}
import scala.xml._
import java.time.LocalDate
import java.time.format.DateTimeFormatter

object H1 {

//  select exmatrikulated students (their studentId) with exmatriculation date
//	only in expected intervall [lowerDate, upperDate]
  def getStudentExList(upperDate: String, lowerDate: String = ""): Option[Seq[Int]] = {
	  val selectStr = s"<$upperDate" +
		  (if (lowerDate != "") s"&>=$lowerDate" else "")

//	val function = Func.findStudent.asInstanceOf[xml.Text]
	  val req = 
        <per:findStudent>
			    <per:disenrollmentDate>
            {selectStr}
          </per:disenrollmentDate>
        </per:findStudent>
    val resp = WsClient.sendMessage(Service.student, req, Func.findStudent)
    resp.map(xml => (xml \\ "studentInfo").map(studInfo => {
      val Seq(studentId, personId) = Seq("studentId","persionId")
       	.map(id => augmentString((studInfo \ id).head.text).toInt)
      personIdFromStudIdCache(studentId) = personId // fill the cache
      studentId
    })).filterNot(_.isEmpty)
//      val pp = new   (80,4)
//      println("AltStudentList response:\n" + pp.format(resp.get))
//      val studentList = (resp.get \\ "id").map(_.text)
//      val idInt = studentList.map(augmentString(_).toInt)
//      println("altStudentList: " + studentList)
      
//    studentList ++= idInt.zip(mtknrInt)
  }

  private val studentInfoCache = collection.mutable.Map[Int,Map[String,Seq[String]]]()
  private val personIdFromUsernameCache = collection.mutable.Map[String,Option[Int]]() 
  private val studentIdFromUsernameCache = collection.mutable.Map[String,Option[Int]]()
  private val personIdFromStudIdCache = collection.mutable.Map[Int,Int]()
  private val form = DateTimeFormatter.ofPattern("yyyy-MM-dd")
  private val today = LocalDate.now
  private val addressTagPrivate = getPrivateAddressTag.getOrElse {
    println("error getting addressTagPrivate")
    sys.exit()
  }
  private val emailTypeId = getEmailTypeId.getOrElse {
    println("error getting emailTypeId")
    sys.exit()
  }
  
//  println("privateAddressTag: " + addressTagPrivate)
//  sys.exit
/*
  def showCacheData = {
    println("personIdFromUsernameCache size: " + personIdFromUsernameCache.size)
    println("personFromStudIdCache size: " + personFromStudIdCache.size)
  }
*/  
  def getPrivateAddressTag: Option[String] = {
    val req = <per:readAddressTagIdForPrivate/>
    val resp = WsClient.sendMessage(Service.address, req, Func.readPrivateAddressTag)
    resp.map(xml => (xml \\ "addressTagId").map(_.text))
      .filterNot(_.isEmpty).map(_.head)
  }

  def getEmailTypeId: Option[String] = {
    val req = <per:readEAddressTypeIdsForEmail/>
    val resp = WsClient.sendMessage(Service.address, req, Func.readEmailTypeId)
    resp.map(xml => (xml \\ "eAddressTypeId").map(_.text))
      .filterNot(_.isEmpty).map(_.head)
  }
  
// read student exmatriculation date from cache or call getStudentInfo to get both data
// and put into cache
  def getStudentExDate(id: Int): LocalDate = {
    val dateStr = studentInfoCache.getOrElseUpdate(id, getStudentInfo(id).get)("disenrollmentDate")
    LocalDate.parse(dateStr.head, form)
  }

// read student username from cache or call getStudentInfo to get both data
// and put into cache
  def getStudentExUsername(id: Int): String = {
    val userList = studentInfoCache.getOrElseUpdate(id, getStudentInfo(id).get)("username")
//    println("getStudentExUsername user: " + user + " with studId: " + id)
//  fill the cache, data is available
    if (userList.length == 1) {
      personIdFromStudIdCache.get(id).map(personId => { 
        val username = userList.head
        studentIdFromUsernameCache(username) = Some(id)
        personIdFromUsernameCache(username) = Some(personId)
      })
    }
    if (userList.isEmpty) "" else userList.head
  }
  
// read exmatriculation date and account (username) in one step
// as key -> value pair in an option
  private def getStudentInfo(id: Int): Option[Map[String,Seq[String]]] = {
	val req = 
      <per:readStudentByStudentId>
			  <per:studentId>{id}</per:studentId>
			  <per:withDegreePrograms>false</per:withDegreePrograms>
			  <per:periodId/>
			  <per:withAddresses>false</per:withAddresses>
       	<per:withAccounts>true</per:withAccounts>
       	<per:withPersonAttributes>false</per:withPersonAttributes>
      </per:readStudentByStudentId>
    val resp = WsClient.sendMessage(Service.student, req, Func.readStudentByStudentId)
    resp.map(xml => Map(ArrowAssoc("disenrollmentDate") -> (xml \\ "disenrollmentDate").map(_.text)) +
        			   // the disenrollmentDate key-> value pair
        (ArrowAssoc("username") -> {
          val userList = (xml \\ "studentAccount") // gives a list of all accounts of one student
            .filter(acc => {
              val Seq(validFrom,validTo) = Seq("validFrom","validTo").map(x => LocalDate.parse((acc \\ x).text, form))
              val valid = today.isAfter(validFrom) && today.isBefore(validTo)
              val validStr = if (valid) "valid" else "invalid"
              val username = (acc \\ "username").text
              val isStudent = username.startsWith("euv")
              if (!isStudent) println(s"found exmatriculated Student with $validStr username: $username")
              isStudent // hold only student accounts maybe valid or invalid in time
            })
            .map(x => (x \\ "username").text)

          if (userList.length > 1) {
            println ("username mit id: " + id + " hat mehr als ein Account: " + userList.mkString(","))
            userList.filter(LdapDB.userExists(_)) // hold only students with Ldap account
          }
          else userList // here is only one account
        }) // the username key -> value pair
      )
  }
  
  private def getPersonIdFromAccount(username:String): Option[Int] = {
    personIdFromUsernameCache.getOrElseUpdate(username, getPersonIdFromAccount2(username))
  }
  
  private def getStudentIdFromAccount(username: String): Option[Int] = {
    studentIdFromUsernameCache.getOrElseUpdate(username, getStudentIdFromAccount2(username))
  }
  
  private def getPersonIdFromAccount2(username: String): Option[Int] = {
    val req = 
        <per:findAccount>
    		  <per:username>{username}</per:username>
    		</per:findAccount>
    val resp = WsClient.sendMessage(Service.account, req, Func.findAccount)
    resp.map(xml => {
      val personIdLst = (xml \\ "personId").map(x => augmentString(x.text).toInt)
//      if (personIdLst.isEmpty) println("found username without personId: " + username)
      personIdLst
    }).filterNot(_.isEmpty).map(_.head)
  }

  private def getStudentIdFromAccount2(username: String): Option[Int] = {
    getPersonIdFromAccount2(username).flatMap(getStudentIdFrompersonId(_))
  }

  private def getStudentIdFrompersonId(personId: Int): Option[Int] = {
    val req =
        <per:findStudent61>
				  <per:personId>{personId}</per:personId>
        </per:findStudent61>
    val resp = WsClient.sendMessage(Service.student, req, Func.findStudent61)
    resp.map(xml => (xml \\ "studentId").map(x => augmentString(x.text).toInt))
      .filterNot(_.isEmpty).map(_.head)
  }
/*  
  def getExmatriculationDate_old(username: String): Option[LocalDate] = {
    val personIdOpt = getPersonIdFromAccount(username)
    personIdOpt.flatMap(getExmaDateByPersonId(_))
//    if (personIdOpt.isDefined) getExmatriculationDate(personIdOpt.get)
//    else None
  }
*/  
  def getExmatriculationDate(username: String): Option[LocalDate] = {
    val studentIdOpt = getStudentIdFromAccount(username)
    studentIdOpt.flatMap(getExmaDateByStudentId(_))
//    if (personIdOpt.isDefined) getExmatriculationDate(personIdOpt.get)
//    else None
  }
/*    
  private def getExmaDateByPersonId(personId: Int): Option[LocalDate] = {
    val req = <per:readStudentWithCoursesOfStudyByPersonId70>
				<per:personId>{personId}</per:personId>
        	</per:readStudentWithCoursesOfStudyByPersonId70>
    val resp = WsClient.sendMessage(Service.student, req, Func.readStudentWithCoursesOfStudyByPersonId70)
    
    resp.map(xml => {
      val dateStrList = (xml \\ "disenrollmentDate").map(_.text)
      if (dateStrList.length > 1) println ("more than one disenrollment date") 
//      if (dateStrList.length == 0) println("no disenrollment date for: " + personId)
      dateStrList.map(LocalDate.parse(_, form))
    }).filterNot(_.isEmpty).map(_.head)
  }
*/
  private def getExmaDateByStudentId(studentId: Int): Option[LocalDate] = {
    val req =
        <per:readStudentByStudentId>
				  <per:studentId>{studentId}</per:studentId>
				  <per:withDegreePrograms>false</per:withDegreePrograms>
				  <per:periodId/>
				  <per:withAddresses>false</per:withAddresses>
         	<per:withAccounts>false</per:withAccounts>
         	<per:withPersonAttributes>false</per:withPersonAttributes>
        </per:readStudentByStudentId>
    val resp = WsClient.sendMessage(Service.student, req, Func.readStudentByStudentId)
    resp.map(xml => {
      val dateStrList = (xml \\ "disenrollmentDate").map(_.text)
      if (dateStrList.length > 1) println ("more than one disenrollment date") 
//      if (dateStrList.length == 0) println("no disenrollment date for: " + personId)
      dateStrList.map(LocalDate.parse(_, form))
    }).filterNot(_.isEmpty).map(_.head)
  }
  
  def getPersonEmail(username: String): Option[String] = {
 	  val idOpt = getPersonIdFromAccount(username)
    idOpt.map(id => {
 	    val req = 
        <per:readEAddresses>
       	  <per:id>{id}</per:id>
 				  <per:objekttype>person</per:objekttype>
        </per:readEAddresses>
      val resp = WsClient.sendMessage(Service.address, req, Func.readEAddresses)
      resp.map(xml => {
            (xml \\ "eAddress").filter(x => ((x \\ "addresstagId").text == addressTagPrivate) &&
                ((x \\ "eaddresstypeId").text == emailTypeId))
            .map(x => (x \\ Person.eaddress.toString).text)
          }).filterNot(_.isEmpty).map(_.head)  
    }).getOrElse(None)
  }
}