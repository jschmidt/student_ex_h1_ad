package object mail {

  import javax.mail._
  import java.util.Properties
  import scala.language.implicitConversions

  implicit def stringToSeq(single: String): Seq[String] = Seq(single)
  implicit def liftToOption[T](t: T): Option[T] = Some(t)

  sealed abstract class MailType
    case object Plain extends MailType
//  case object Rich extends MailType
    case object MultiPart extends MailType

  case class Mail(
    from: (String, String), // (email -> name)
    to: Seq[String],
    cc: Seq[String] = Seq.empty,
    bcc: Seq[String] = Seq.empty,
    subject: String,
    message: String,
//    richMessage: Option[String] = None,
    attachment: Option[(java.nio.file.Path)] = None
  )
  
  val prop = System.getProperties
  prop.setProperty("mail.smtp.host", main.scala.jdbc.Config.getProperty("Mail_host"))
  System.setProperties(prop)

  object send {
    def a(mail: Mail): Unit = {
      import org.apache.commons.mail.{Email, SimpleEmail, MultiPartEmail, EmailAttachment}
      val format =
        if (mail.attachment.isDefined) MultiPart
        else Plain

//      val commonsMail: Email = new SimpleEmail().setMsg(mail.message)
      val commonsMail: Email = format match {
        case Plain => new SimpleEmail().setMsg(mail.message)
        case MultiPart => {
          val attachment = new EmailAttachment()
          val attFile = mail.attachment.get.toFile
          attachment.setPath(attFile.getAbsolutePath)
          attachment.setDisposition(EmailAttachment.ATTACHMENT)
          attachment.setName(attFile.getName)
          new MultiPartEmail().attach(attachment).setMsg(mail.message)
        }
      }

      // TODO Set authentication from your configuration, sys properties or w/e

      //      val session = Session.getDefaultInstance(prop)
      // Can't add these via fluent API because it produces exceptions
      mail.to foreach (commonsMail.addTo(_))
      mail.cc foreach (commonsMail.addCc(_))
      mail.bcc foreach (commonsMail.addBcc(_))

      commonsMail
        .setFrom(mail.from._1, mail.from._2)
        .setSubject(mail.subject)
        .send()
    }
  }
}