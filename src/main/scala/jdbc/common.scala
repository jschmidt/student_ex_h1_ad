package main.scala.jdbc
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.logging.{Logger,FileHandler,SimpleFormatter}
import java.io.IOException
import java.nio.file.{Paths,Files}

object Role extends Enumeration {
  val student, mitarbeiter = Value
}

object LogObj {

  val append = true
  val dateFormLog = DateTimeFormatter.ofPattern("yyyy_MM_dd")
  val dateStr = LocalDate.now.format(dateFormLog)
  val fileName0 = s"studentEx${dateStr}.log"
  val fileName = Paths.get(System.getProperty("user.home"),"accountlog",fileName0)
  val dirName = fileName.getParent
  if (!Files.exists(dirName)) Files.createDirectory(dirName)
  
  def modLogger(className: String): Logger = {
    val logObj = Logger.getLogger(className) 
    try {
      val fh = new FileHandler(fileName.toString, append)
      fh.setFormatter(new SimpleFormatter)
      logObj.addHandler(fh)
    }
    catch {
      case e1: SecurityException => e1.printStackTrace;
      case e2: IOException => e2.printStackTrace;
    }
    logObj
  }
}

object Ou extends Enumeration {
//  val Studenten, Mitarbeiter = Value
  val Studenten = Value
  val Studenten_ex = Value("Studenten-ex")
  val Nach_2M = Value("Nach- 2M")
  val Nach_6M = Value("Nach- 6M")
  val Nach_12M = Value("Nach-12M")
//  val EUV_Mitarbeiter = Value("EUV Mitarbeiter")
//  val EUV_Zwischen = Value("EUV Zwischen")
}

object Service extends Enumeration {
  val account = Value("AccountService")
  val address = Value("AddressService")
  val person = Value("PersonService")
  val rightsAndRoles = Value("RightsAndRolesService")
  val student = Value("StudentService")
}

object Func extends Enumeration {
  val findStudent, findStudent61, findRoles60, findPerson, findPerson60, readRolesForPerson = Value
  val readStudentByStudentId, readStudentWithCoursesOfStudyByPersonId70 = Value
  val readPerson, searchAccountForPerson, findAccount, createNewAccountForPerson = Value
  val readPrivateAddressTag = Value("readAddressTagIdForPrivate")
  val readEmailTypeId = Value("readEAddressTypeIdsForEmail")
  val readEAddresses = Value
}

object Person extends Enumeration {
//  val surname, firstname, dateofbirth = Value
  val disenrollmentDate, username = Value
  val eaddress = Value
}

object Utils {
  import java.text.Normalizer
  import util.matching.Regex
  def filter(str: String): String = {
    val sb = augmentString(str).map(ch => ch match {
      case 'Ä' => "Ae" case 'ä' => "ae"
      case 'Ö' => "Oe" case 'ö' => "oe"
      case 'Ü' => "Ue" case 'ü' => "ue"
      case 'ı' => "i"	// turkish
      case 'ł' => "l"	// polish
      case _ => ch
    })
    val str1 = Normalizer.normalize(sb.mkString,Normalizer.Form.NFD)
    new Regex("\\p{Mn}+").replaceAllIn(str1,"")	// Mn -> Non_Spacing_Mark remove
  }

  def pw_check(pw: String): Boolean = {
    val patternList = Seq(new Regex("\\d+"), new Regex("[A-Z]+"),
          new Regex("[a-z]+"), new Regex("[-+!@#$%&/=_.,:;]+"))
    @annotation.tailrec
    def check(regList: Seq[Regex], sum: Int): Boolean = {
      if (sum <= 0) true
      else {
        if (regList.isEmpty) false
        else {
          val sum1 = if (!regList.head.findFirstIn(pw).isEmpty) sum - 1 else sum
          check(regList.tail, sum1)
        }
      }
    }
    if (pw.length < 8) false else check(patternList, 3)
  }
}