package main.scala.jdbc

import java.util.Properties
//import java.io.IOException;
import javax.naming.directory.{SearchControls, DirContext}
import javax.naming.{Context, NamingException}
import javax.naming.ldap.{LdapContext, InitialLdapContext}
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit

//class LdapDB(adminName: String = "harbormaster@his-ad.euv", adminPassword: String = "1234Max") {
//class LdapDB(adminName: String = "his@ad.uni-ffo.de", adminPassword: String = "1743qPr!") {
object LdapDB {
//	val searchBase = "OU=Studenten"
	private val Seq(adminName,adminPassword,ldapURL) =
	  Seq("AD_admin","AD_password","AD_host").map(Config.getProperty(_))
  val searchCtls = new SearchControls;
	
//	val actualYear = Calendar.getInstance.get(Calendar.YEAR)
	val actualYear = LocalDate.now.getYear
  val logger = LogObj.modLogger(this.getClass.getName)
	val ctx = init
	
//	some useful constants from lmaccess.h
	val UF_ACCOUNTDISABLE = 0x0002;
	val UF_PASSWD_NOTREQD = 0x0020;
	val UF_PASSWD_CANT_CHANGE = 0x0040;
	val UF_NORMAL_ACCOUNT = 0x0200;
	val UF_DONT_EXPIRE_PASSWD = 0x10000;
	val UF_PASSWORD_EXPIRED = 0x800000;
	
	def init: LdapContext = {
		val env = new Properties
		// 636 is ldaps
		//val ldapURL = "ldaps://10.173.74.128:636/DC=his-ad,DC=euv"
		//val ldapURL = "ldaps://viadc07.ad.uni-ffo.de:636/DC=ad,DC=uni-ffo,DC=de"
//		val ldapURL = "ldaps://ad.uni-ffo.de:636/DC=ad,DC=uni-ffo,DC=de"
//		val ldapURL = config.getProperty("AD_host")
		
		println("ldapURL: " + ldapURL + " admin: " + adminName)

		env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
//	    env.put(Context.SECURITY_PROTOCOL, "ssl");
		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		env.put(Context.SECURITY_PRINCIPAL, adminName);
		env.put(Context.SECURITY_CREDENTIALS, adminPassword);

		// connect to my domain controller
		env.put(Context.PROVIDER_URL, ldapURL);

		// Create the search controls
		// Specify the search scope
		searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		// Specify the Base for the search
		// searchBase = "DC=his-ad,DC=euv"; // now is in ldapURL
		// Specify the attributes to return
		//val returnedAtts =  Seq("mail","SN")
		//searchCtls.setReturningAttributes(returnedAtts.toArray);

//		Certificate is in viacampus.jks included
//		println("property: " + System.getProperty("javax.net.ssl.trustStore"))
//		System.setProperty("javax.net.ssl.trustStore", "conf/viacampus.jks" );
//		System.setProperty("javax.net.ssl.trustStorePassword", "viacampus" );
//		System.setProperty("javax.net.debug", "handshake")
		
		new InitialLdapContext(env, null)
/*		if (ldapURL startsWith("ldap")) {
		  val tls = ctx.extendedOperation(new StartTlsRequest()).asInstanceOf[StartTlsResponse]
		  val session = tls.negotiate()
		  println("session: " + session)
		} */
	}

  def userExists(uid: String): Boolean = {
    ctx.search("", "sAMAccountName=" + uid, searchCtls).hasMoreElements
	}

  def userExistsInOU(uid: String, ou: Ou.Value): Boolean = {
//      println("userExistsInOU with: " + uid + " " + ou)
//      userExistsInOU(uid,Seq(getUserOU(ou)))
	  val userOUList = getUserOU(ou).split(",", 2)
	  val targetOU = userOUList(0)	// .head
    if (ctx.search(userOUList(1), targetOU, searchCtls).hasMoreElements)
      ctx.search(getUserOU(ou), "sAMAccountName=" + uid, searchCtls).hasMoreElements
    else false
  }
    
  def userExistsInOUList(uid: String, ouList: Seq[Ou.Value]): Boolean = {
    @annotation.tailrec
    def iter(uid: String, ouL1: Seq[Ou.Value], res: Boolean): Boolean = {
      if (res || ouL1.isEmpty) res
      else {
        val result = ctx.search("OU=" + ouL1.head, "sAMAccountName=" + uid, searchCtls)
        iter(uid, ouL1.tail, result.hasMoreElements)
      }
    }
    iter(uid, ouList, false)
	}

/*    def userExistsInOU(uid: String, ou: Ou.Value): Boolean = {
//      println("userExistsInOU with: " + uid + " " + ou)
//      userExistsInOU(uid,Seq(getUserOU(ou)))
	  val userOUList = getUserOU(ou).split(",", 2)
	  val targetOU = userOUList(0)	// .head
      if (ctx.search(userOUList(1), targetOU, searchCtls).hasMoreElements)
        ctx.search(getUserOU(ou), "sAMAccountName=" + uid, searchCtls).hasMoreElements
      else false
    }
*/    

	import javax.naming.directory.{BasicAttribute, BasicAttributes}

	private val ouList = collection.mutable.Set[String]()	// OU cache
//	  only create/search targetOU once per run
  def ouCreate(userOU: String): Unit = {
	  val userOUList = userOU.split(",", 2)
	  val targetOU = userOUList(0)	// .head
	  if(!ouList.contains(targetOU)) {
      val ouExist = ctx.search(userOUList(1), targetOU, searchCtls).hasMoreElements
			if (!ouExist) {
		  	val attrs = new BasicAttributes(true)
		  	val objclass = new BasicAttribute("objectClass")
		  	objclass.add("top"); objclass.add("organizationalUnit")
		  	attrs.put(objclass)
		  	val result = ctx.createSubcontext(userOU, attrs)
		  	println("OU: " + userOU + " created, result: " + result)
		  	println("name: " + result.getNameInNamespace)
			}
      ouList += targetOU
	  }
  }
	
/*	def getUidFromName(firstName: String, lastName: String): Seq[String] = {
	  @annotation.tailrec
	  def getUidFromName(firstName: String, lastName: String, ouStr: Seq[Ou.Value], result: Seq[String]): Seq[String] = {
	    if (ouStr.isEmpty) result
	    else {
//	      val searchFilter = "(&(sn=" + lastName + ")(givenName=" + firstName + "))"
	      val searchFilter = s"(&(sn=$lastName)(givenName=$firstName))"
	      val attributes = Array("sAMAccountName")
	      val altSearchCtls = searchCtls
	      altSearchCtls.setReturningAttributes(attributes)
	      val searchBaseStr = "OU=" + ouStr.head
	  
	      val answer = ctx.search(searchBaseStr, searchFilter, altSearchCtls)

	      val uidList = new collection.mutable.ListBuffer[String]
	      while (answer.hasMore) {
	        uidList += answer.next.getAttributes.get("sAMAccountName").get(0).toString
	      }
	      getUidFromName(firstName,lastName,ouStr.tail,Seq.concat(result,uidList.toSeq))
	    }
	  } // end of iterating function getUidFromName(_,_,_,_)
	  getUidFromName(firstName,lastName,Seq(Ou.EUV_Mitarbeiter,Ou.Mitarbeiter),Seq())
	}
*/	
	def moveUser(username: String, targetOU: Ou.Value): Unit = {
	  val attributes = Array("distinguishedName")
	  val altSearchCtls = searchCtls
	  altSearchCtls.setReturningAttributes(attributes)
//	  val cn = ctx.getAttributes(username, Array("distinguishedName"))
	  val answer = ctx.search("", "sAMAccountName=" + username, altSearchCtls)
	  val dn0 = answer.next.getAttributes.get("distinguishedName").get.toString
	  val dn = dn0.split(",DC=", 2)(0)	// second part not used!
	  val newdn0 = dn.split(",OU=", 2)
	  val (newdn, newdn1) = (newdn0(0), "OU=" + newdn0(1))

//	this is our AD Administrator, the only person to communicate -> AcceptMessagesOnlyFrom
//	TODO select a matching user from your AD
    val answer2 = ctx.search("", "sAMAccountName=jstarobrat", altSearchCtls)
	  val jola_dn = answer2.next.getAttributes.get("distinguishedName").get.toString
//	  println("Jola: " + jola_dn)

	  val attList = new BasicAttributes(true)
	  if (Seq(Ou.Nach_6M,Ou.Nach_12M).contains(targetOU)) {
//	  set restrictions: Exchange Parameters AcceptMessagesOnlyFrom and HiddenFromAdressList
	    attList.put(new BasicAttribute("msExchHideFromAddressLists","TRUE"))
	    attList.put(new BasicAttribute("authOrig",jola_dn))
//	    val UF_ACCOUNTDISABLE = 0x2  // set AccountDisable attribute
	    val userAccountControlOrig = augmentString(ctx.getAttributes(dn).get("userAccountControl").get.toString).toInt
//	    println("userAccountControlOrig: " + userAccountControlOrig)
	    attList.put(new BasicAttribute("userAccountControl",(userAccountControlOrig | UF_ACCOUNTDISABLE).toString))
	  }
	  
	  try {
	    if (attList.size > 0) {
//	      println("ctx.modifyAttributes of: " + dn)
	      ctx.modifyAttributes(dn, DirContext.REPLACE_ATTRIBUTE, attList)
	    }
	    val newOuString = getUserOUMove(targetOU)
	    ouCreate(newOuString)
//	    println("ctx.rename")
	    ctx.rename(dn, newdn + "," + newOuString)
	    val str = "Account: " + username + " verschoben von: " + newdn1 + " nach: " + newOuString
	    println(str)
	    logger.info(str)
	  }
	  catch {
			case e: NamingException => {
	      val tempStr = "Problem bei der Objekt Verschiebung für: " + dn + " " + e
		  	println(tempStr);
		  	logger.info("<Fehler> " + tempStr)
//		  sys.exit(1)
			}
	  }
	}

	def getExpirationDate(username: String): LocalDate = {
	  val altSearchCtls = searchCtls
	  altSearchCtls.setReturningAttributes(Array("accountExpires"))
	  val answer = ctx.search("", "sAMAccountName=" + username, altSearchCtls)
//	  get 100nanosecond ticks since 01.01.1601
	  val expTicks = answer.next.getAttributes.get("accountExpires").get.toString
	  
//	  val form = DateTimeFormatter.ofPattern("yyyy-MM-dd")

//	  convert 100ns ticks to seconds
	  val expSeconds = augmentString(expTicks).toLong / 10000000
//	  val javaTime = ticks_ms - 11644473600000l // offset milliseconds from Jan 1, 1601 to Jan 1, 1970
	  val w32Start = LocalDate.of(1601, 1, 1)

//	  val exp = Instant.ofEpochMilli(javaTime).atZone(ZoneId.systemDefault).toLocalDate
//	  
	  val expDate = w32Start.atStartOfDay.plusSeconds(expSeconds).toLocalDate
//	  println("date: " + expDate.format(form))
	  expDate
	}
	
	def setExpirationDate(username: String, expDate: LocalDate): Unit = {
//	  println("expected expiration date: " + expDate)
	  val altSearchCtls = searchCtls
	  altSearchCtls.setReturningAttributes(Array("distinguishedName"))
//	  val cn = ctx.getAttributes(username, Array("distinguishedName"))
	  val answer = ctx.search("", "sAMAccountName=" + username, altSearchCtls)
	  val dn0 = answer.next.getAttributes.get("distinguishedName").get.toString
	  val dn = dn0.split(",DC=", 2)(0)	// second part not used!

	  val w32Start = LocalDate.of(1601, 1, 1).atStartOfDay
	  val ticks100ns = w32Start.until(expDate.atStartOfDay, ChronoUnit.MILLIS) * 10000

	  val attList = new BasicAttributes(true)
	  attList.put(new BasicAttribute("accountExpires",ticks100ns.toString))
	  try ctx.modifyAttributes(dn, DirContext.REPLACE_ATTRIBUTE, attList)
	  catch {
			case e: NamingException => {
	      val tempStr = "problem setting expiration date for: " + dn + " " + e
		  	println(tempStr)
			}
	  }
	}
	
	import javax.naming.ldap.PagedResultsResponseControl
	private def response: Option[Array[Byte]] = {
	  val controls = ctx.getResponseControls //getResponseControls getRequestControls
	  if (controls != null && controls.length == 1) {
//	    val cookie = controls(0).asInstanceOf[PagedResultsResponseControl].getCookie
	    val cookie = controls(0) match { // the scala way here
	      case x: PagedResultsResponseControl => x.getCookie
	      case _ => null
	    }
	    if (cookie != null) Some(cookie) else None
	  }
	  else { 
	    println("no ResponseControl dataset")
	    None
	  }
	} // end of def response
	
	import javax.naming.NamingEnumeration
	import javax.naming.directory.SearchResult
	import javax.naming.ldap.{PagedResultsControl,Control}
  def getAllFromOU(OuValue: Ou.Value): Seq[String] = {
	  val tempSearchCtls = searchCtls
	  tempSearchCtls.setReturningAttributes(Array("sAMAccountName"))
	  val pageSize = 100
	  val searchBase0 = "OU=" + OuValue

	  @annotation.tailrec
	  def iter(list: NamingEnumeration[SearchResult], result: Seq[String]): Seq[String] = {
			if (list.hasMoreElements) {
		  	val username = list.next.getAttributes.get("sAMAccountName").get.toString
		  	iter(list, username +: result)
			}
			else {
		  	val listopt = response.map(cookie => {
		    	ctx.setRequestControls(Array(new PagedResultsControl(pageSize,cookie,Control.CRITICAL)))
		    	ctx.search(searchBase0, "sAMAccountName=*", tempSearchCtls)
		  	})
		  	if (listopt.isDefined) iter(listopt.get, result)
		  	else { // that is all now
		    	ctx.setRequestControls(Array(new PagedResultsControl(pageSize,Control.NONCRITICAL)))
		    	result
		  	}
			}
	  } // end iter

	  ctx.setRequestControls(Array(new PagedResultsControl(pageSize,Control.NONCRITICAL)))
	  val answer = ctx.search(searchBase0, "sAMAccountName=*", tempSearchCtls)
	  iter(answer, Seq())
	}

//	use other OU on move
	def getUserOUMove(targetOu: Ou.Value): String = {
//	  getUserOU(targetOu, actualYear + "-Test")
//	  getUserOU(targetOu, Ou.Studenten_ex)
	  "OU=" + targetOu + ",OU=" + Ou.Studenten_ex
	}
	
	def getUserOU(targetOu: Ou.Value): String = {
	  getUserOU(targetOu, actualYear.toString)
//	  getUserOU(targetOu,"Nach-2M")
	}
	
	def getUserOU(targetOu: Ou.Value, testOU: String): String = {
	  val exmaStr = "OU=" + targetOu + ",OU=" + Ou.Studenten_ex
	  val testOUStr = "OU=" + testOU + ",OU=" + targetOu
	  targetOu match {
	    case Ou.Studenten => testOUStr
	    case Ou.Studenten_ex => testOUStr
	    case Ou.Nach_2M => exmaStr
	    case Ou.Nach_6M => exmaStr
	    case Ou.Nach_12M => exmaStr
//	    case Ou.EUV_Mitarbeiter => "OU=" + Ou.EUV_Zwischen + ",OU=" + targetOu
	  }
	}
	
	def getUserDN(cnValue: String, targetOu: Ou.Value) = "CN=" + cnValue + "," + getUserOU(targetOu)
	
	def cnValueExt(userName: String, targetOu: Ou.Value): String = {
	  targetOu match {
	    case Ou.Studenten => " (" + userName + ")"
//	    case Ou.EUV_Mitarbeiter  => ""
	  }
	}
}